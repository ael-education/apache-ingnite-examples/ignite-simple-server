#!/bin/bash
# Каталоги инсталляции

CONFIG_FILE=/home/vaganovdv/temp/ignite/application.yml

# Конфигурация опций JVM
configJVMOptions() {
  #
  # JVM options. See http://java.sun.com/javase/technologies/hotspot/vmoptions.jsp for more details.
  #
  # ADD YOUR/CHANGE ADDITIONAL OPTIONS HERE
  #
  if [ -z "$JVM_OPTS" ]; then
    JVM_OPTS="-Xms1g -Xmx1g -server -XX:MaxMetaspaceSize=256m"
  fi
  #
  # Uncomment the following GC settings if you see spikes in your throughput due to Garbage Collection.
  #
  # JVM_OPTS="$JVM_OPTS -XX:+UseG1GC"

  #
  # Uncomment if you get StackOverflowError.
  # On 64 bit systems this value can be larger, e.g. -Xss16m
  #
  # JVM_OPTS="${JVM_OPTS} -Xss4m"

  #
  # Uncomment to set preference for IPv4 stack.
  #
  JVM_OPTS="${JVM_OPTS} -Djava.net.preferIPv4Stack=true"

  #
  # Assertions are disabled by default since version 3.5.
  # If you want to enable them - set 'ENABLE_ASSERTIONS' flag to '1'.
  #
  ENABLE_ASSERTIONS="0"
  #
  # Set '-ea' options if assertions are enabled.
  #
  if [ "${ENABLE_ASSERTIONS}" = "1" ]; then
    JVM_OPTS="${JVM_OPTS} -ea"
  fi
}

# Проверка инсталляции JAVA
function checkJavaCorrectInstall() {
  if type -p java; then
    echo "Обнаружены исполняемые файлы JAVA в переменной PATH"
    _java=java
  elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]]; then
     echo "Обнаружены исполняемые файлы JAVA в каталоге JAVA_HOME "
     _java="$JAVA_HOME/bin/java"
  else
    echo "Виртуальная машина JAVA не обнаружена"
    exit 1
  fi

}

function configureJVMversion() {
  #
  # Final JVM_OPTS for Java 9+ compatibility
  #
  printf "Версия JAVA: [ %s ]\n" $version

  if [ $version -eq 8 ]; then
    JVM_OPTS="\
        -XX:+AggressiveOpts \
         ${JVM_OPTS}"

  elif [ $version -gt 8 ] && [ $version -lt 11 ]; then
    JVM_OPTS="\
        -XX:+AggressiveOpts \
        --add-exports=java.base/jdk.internal.misc=ALL-UNNAMED \
        --add-exports=java.base/sun.nio.ch=ALL-UNNAMED \
        --add-exports=java.management/com.sun.jmx.mbeanserver=ALL-UNNAMED \
        --add-exports=jdk.internal.jvmstat/sun.jvmstat.monitor=ALL-UNNAMED \
        --add-exports=java.base/sun.reflect.generics.reflectiveObjects=ALL-UNNAMED \
        --illegal-access=permit \
        --add-modules=java.xml.bind \
        ${JVM_OPTS}"

  elif [ $version -ge 11 ]; then
    JVM_OPTS="\
        --add-exports=java.base/jdk.internal.misc=ALL-UNNAMED \
        --add-exports=java.base/sun.nio.ch=ALL-UNNAMED \
        --add-exports=java.management/com.sun.jmx.mbeanserver=ALL-UNNAMED \
        --add-exports=jdk.internal.jvmstat/sun.jvmstat.monitor=ALL-UNNAMED \
        --add-exports=java.base/sun.reflect.generics.reflectiveObjects=ALL-UNNAMED \
        --add-opens jdk.management/com.sun.management.internal=ALL-UNNAMED \
        --illegal-access=permit \
        ${JVM_OPTS}"
  fi
  printf "JVM: %s \n" $JVM_OPTS
  printf "Приложение готово к запуску .... \n"
}



function startApplication() {  
  printf "Cтарт сервера: ..."
  printf "\n"  
  printf "Конфигурация: [%s]  %s \n" "$CONFIG_HOME/$CONFIG_FILE"
  java -jar ignite-simple-server-0.0.1.jar \
  --spring.config.location="file:$CONFIG_FILE" \
 ${JVM_OPTS}
}


printf "Конфигурация JAVA ... \n"
checkJava
checkJavaCorrectInstall
printf "Конфигурация JVM OPTIONS ... \n"
configJVMOptions
configureJVMversion
startApplication
